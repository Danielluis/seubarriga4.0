# Seu Barriga


### Tecnologia

Site que eu peguei as tecnologia: https://mvnrepository.com/
* Java 
* JUnit 5 
* Selenium
* Bonigarcia
### Installation

Install the dependencies para inicia os testes.

```sh
* JUnit Jupiter API = versão 5.6.1
* JUnit Jupiter Engine = versão 5.6.1
* Selenium Java = versão 4.0.0-alpha-5
* WebDriverManager  = versão  3.8.1
```
### Descrição

```sh
- Com os pacotes AppObjects, Tasks, TestCases.

Classes:

- Com classe loginAppObjects é o mapeamento quando eu faço o login no site no meu teste eu fiz duas situação um que eu não preencho os campos obrigatórios do login e confirmo as mensagem que aparece e
faço o login com sucesso e confirmo a mensagem que mostra quando 
logar e na loginTasks é as ações da execução de cada mapeamento.

- Com a classes menuAppObjects e menuTasks eu fiz o mapeamento onde eu vou para a pagina de adicionar conta.

- Com a classes criarContaAppObjects e criarContaTasks nessa classe eu fiz duas situação primeiro eu criei a conta com sucesso e confirmei a mensagem e em seguida eu criei outra conta com mesmo nome e confirmei a mensagem que aparece.

- Com a classes movimentacaoAppObjects e movimentacaoTasks é quando eu vou ate a pagina de criar movimentação e preencho os campos 
necessário e salvo a movimentação

- Com a classes homeAppObjects e homeTasks quando eu vou para o home e verificar que a movimentação foi salvo com sucesso e 
verificando no nome e o valor.

- Com a classes removendoMovimentacao e removendoMovimentacao 
quando eu vou pro resumo mensal e localizo a movimentação e eu exclui-o ela e eu deslogo do site  

```