package tasks;

import appObjects.removendoMovimentacaoAppObjects;
import org.openqa.selenium.WebDriver;

public class removendoMovimentacaoTasks {


    private WebDriver driver;
    private removendoMovimentacaoAppObjects removendoMovimentacaoAppObjects;

    public removendoMovimentacaoTasks(WebDriver driver){
        this.driver = driver;
        this.removendoMovimentacaoAppObjects = new removendoMovimentacaoAppObjects(driver);
    }

    public void excluirConta(){
        removendoMovimentacaoAppObjects.contas().click();
        removendoMovimentacaoAppObjects.lista().click();
        removendoMovimentacaoAppObjects.excluirConta().click();
        removendoMovimentacaoAppObjects.sair().click();
    }
}
