package tasks;

import appObjects.criarContaAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class criarContaTasks {


    private WebDriver driver;
    private criarContaAppObjects criarContaAppObjects;

    public criarContaTasks(WebDriver driver){
        this.driver = driver;
        this.criarContaAppObjects = new criarContaAppObjects(driver);
    }

    public void criarConta(){
        criarContaAppObjects.nome().sendKeys("Inter Banco");
        criarContaAppObjects.botaoSalvar().click();
        Assertions.assertEquals("Conta adicionada com sucesso!",  criarContaAppObjects.mensagemSucesso().getText());

    }

    public void contaMesmoNome(){
        criarContaAppObjects.nome().sendKeys("Inter Banco");
        criarContaAppObjects.botaoSalvar().click();
        Assertions.assertEquals("Já existe uma conta com esse nome!", criarContaAppObjects.mensagemnegativa().getText());
    }

}
