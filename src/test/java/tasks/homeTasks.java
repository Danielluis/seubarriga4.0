package tasks;

import appObjects.homeAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class
homeTasks {

    private WebDriver driver;
    private homeAppObjects homeAppObjects;

    public homeTasks(WebDriver driver){
        this.driver = driver;
        this.homeAppObjects = new homeAppObjects(driver);
    }

    public void validacaoConta() {
        homeAppObjects.botaoHome().click();
        Assertions.assertEquals("Inter Banco", homeAppObjects.validarConta().getText());
    }
    public void validacaoValor(){
        Assertions.assertEquals("150000.00", homeAppObjects.validarValor().getText());


    }
}
