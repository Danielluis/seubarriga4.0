package tasks;

import appObjects.loginAppObjects;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class loginTasks {


    private WebDriver driver;
    private loginAppObjects loginAppObjects;

    public loginTasks(WebDriver driver){
        this.driver = driver;
        this.loginAppObjects = new loginAppObjects(driver);
    }

    public void loginComCamposVazios(){
        loginAppObjects.botaoEntrar().click();
        Assertions.assertEquals("Email é um campo obrigatório",loginAppObjects.mensagemEmailNegativo().getText());
        Assertions.assertEquals("Senha é um campo obrigatório",loginAppObjects.mensagemSenhaNegativo().getText());

    }

    public void login(){
//        WebDriverWait wait = new WebDriverWait(driver,30);
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='navbar-brand']")));
        loginAppObjects.email().sendKeys("daniel.luis.siqueira@gmail.com");
        loginAppObjects.senha().sendKeys("19121995");
        loginAppObjects.botaoEntrar().click();
        Assertions.assertEquals( "Bem vindo, Daniel !",loginAppObjects.mensagemSucesso().getText());
    }
}
