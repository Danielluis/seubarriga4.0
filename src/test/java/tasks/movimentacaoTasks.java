package tasks;

import appObjects.movimentacaoAppObjects;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class movimentacaoTasks {

    private WebDriver driver;
    private movimentacaoAppObjects movimentacaoAppObjects;

    public movimentacaoTasks(WebDriver driver){
        this.driver = driver;
        this.movimentacaoAppObjects = new movimentacaoAppObjects(driver);

    }

    public void movimentacao(){
        movimentacaoAppObjects.botaoCriarMovimentacao().click();
        movimentacaoAppObjects.dataMovimentacao().sendKeys("01/05/2018");
        movimentacaoAppObjects.dataPagmento().sendKeys("01/05/2018");
        movimentacaoAppObjects.descricao().sendKeys("Teste automação 13/03/2020");
        movimentacaoAppObjects.interecado().sendKeys("Daniel");
        movimentacaoAppObjects.valor().sendKeys("150000");
        movimentacaoAppObjects.situacao().click();
        movimentacaoAppObjects.botaoSalvarMovimentacao().click();
        Assertions.assertEquals("Movimentação adicionada com sucesso!",movimentacaoAppObjects.mensagemMovimentacaoSucesso().getText());
    }
}
