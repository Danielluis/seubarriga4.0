package tasks;

import appObjects.menuAppObjects;
import org.openqa.selenium.WebDriver;

public class menuTasks {


    private WebDriver driver;
    private menuAppObjects menuAppObjects;

    public menuTasks(WebDriver driver){
        this.driver = driver;
        this.menuAppObjects = new menuAppObjects(driver);
    }

    public void conta() {
        menuAppObjects.contas().click();
        menuAppObjects.adicionar().click();
    }
}
