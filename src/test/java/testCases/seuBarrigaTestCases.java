package testCases;

import appObjects.loginAppObjects;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import tasks.*;

public class seuBarrigaTestCases {

    private WebDriver driver;
    private loginTasks loginTasks;
    private menuTasks menuTasks;
    private criarContaTasks criarContaTasks;
    private movimentacaoTasks movimentacaoTasks;
    private homeTasks homeTasks;
    private removendoMovimentacaoTasks removendoMovimentacaoTasks;

    @BeforeEach
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
        this.driver.get("https://seubarriga.wcaquino.me/");
        this.loginTasks = new loginTasks(driver);
        this.menuTasks = new menuTasks(driver);
        this.criarContaTasks = new criarContaTasks(driver);
        this.movimentacaoTasks = new movimentacaoTasks(driver);
        this.homeTasks = new homeTasks(driver);
        this.removendoMovimentacaoTasks = new removendoMovimentacaoTasks(driver);
    }

    @AfterEach
    public void tearDow(){
//        driver.quit();
    }

    @Test
    public void teste(){
        loginTasks.loginComCamposVazios();
        loginTasks.login();
        menuTasks.conta();
        criarContaTasks.criarConta();
        menuTasks.conta();
        criarContaTasks.contaMesmoNome();
        movimentacaoTasks.movimentacao();
        homeTasks.validacaoConta();
        homeTasks.validacaoValor();
        removendoMovimentacaoTasks.excluirConta();
    }

}
