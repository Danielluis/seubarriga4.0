package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class homeAppObjects {

    private WebDriver driver;

    public homeAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement botaoHome(){
        return driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
    }

    public WebElement validarConta(){
        return driver.findElement(By.xpath("//td[contains(text(),'Inter Banco')]"));
    }

    public WebElement validarValor(){
        return driver.findElement(By.xpath("//td[contains(text(),'150000.00')]"));
    }

 }
