package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class movimentacaoAppObjects {

    private WebDriver driver;

    public movimentacaoAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement botaoCriarMovimentacao(){
        return driver.findElement(By.linkText("Criar Movimentação"));
    }

    public WebElement dataMovimentacao(){
        return driver.findElement(By.id("data_transacao"));

    }

    public WebElement dataPagmento(){
        return driver.findElement(By.id("data_pagamento"));
    }

    public WebElement descricao(){
        return driver.findElement(By.id("descricao"));
    }

    public WebElement interecado(){
        return driver.findElement(By.id("interessado"));
    }
    public WebElement valor(){
        return driver.findElement(By.id("valor"));
    }

    public WebElement situacao(){
        return driver.findElement(By.id("status_pago"));
    }

    public WebElement botaoSalvarMovimentacao(){
        return driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
    }
    public WebElement mensagemMovimentacaoSucesso(){
        return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
    }


}
