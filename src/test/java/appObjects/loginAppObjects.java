package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class loginAppObjects {


        private WebDriver driver;

        public loginAppObjects(WebDriver driver){
            this.driver = driver;

        }

        public WebElement email(){
            return driver.findElement(By.id("email"));
        }

        public WebElement senha(){
            return driver.findElement(By.id("senha"));
        }

        public WebElement botaoEntrar(){
            return driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
        }

        public WebElement mensagemSucesso(){
            return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
        }

        public WebElement mensagemEmailNegativo(){
             return driver.findElement(By.xpath("//div[contains(text(),'Email é um campo obrigatório')]"));
        }
        public WebElement mensagemSenhaNegativo(){
            return driver.findElement(By.xpath("//div[contains(text(),'Senha é um campo obrigatório')]"));
        }

}
