package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class criarContaAppObjects {

    private WebDriver driver;

    public criarContaAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement nome(){
        return driver.findElement(By.id("nome"));
    }

    public WebElement botaoSalvar(){
        return driver.findElement(By.xpath("//button[@class='btn btn-primary']"));
    }

    public  WebElement mensagemSucesso(){
        return driver.findElement(By.xpath("//div[@class='alert alert-success']"));
    }

    public WebElement mensagemnegativa(){
        return driver.findElement(By.xpath("//div[@class='alert alert-danger']"));
    }
}
