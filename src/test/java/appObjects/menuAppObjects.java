package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class menuAppObjects {


    private WebDriver driver;

    public menuAppObjects(WebDriver driver){
        this.driver = driver;
    }

    public WebElement contas(){
        return driver.findElement(By.className("dropdown-toggle"));
    }

    public WebElement adicionar(){
        return driver.findElement(By.linkText("Adicionar"));
    }
}
