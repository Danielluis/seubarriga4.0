package appObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class removendoMovimentacaoAppObjects {


    private WebDriver driver;

    public removendoMovimentacaoAppObjects(WebDriver driver){
        this.driver = driver;
    }



    public WebElement contas(){
        return driver.findElement(By.xpath("//a[@class='dropdown-toggle']"));
    }

    public WebElement lista(){
        return driver.findElement(By.xpath("//a[contains(text(),'Listar')]"));
    }

    public WebElement excluirConta(){
        return driver.findElement(By.xpath("//span[@class='glyphicon glyphicon-remove-circle']"));
    }

    public WebElement sair() {
        return driver.findElement(By.xpath("//a[contains(text(),'Sair')]"));

    }

}
